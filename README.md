
# Formalist Classic Style

The classic style for a site that follows the [Formalist site format,](https://github.com/sunsetworks/formalist-site-format)  implemented as [CSS rules in style.css.](style.css)

## Purpose of This Document

This document is meant to specify the classic style that is used by default for all Formalist sites. 

## Design Concept

Formalist is meant to be like a gallery for your content. The classic style is a simple, modern, clean design that gets out of your way. The colors are a little bit of old and a little bit of new. 

The classic style uses whichever modern sans-serif font is available on the device, so the text blends in with its surroundings and feels more like a general-purpose utility tool. 
